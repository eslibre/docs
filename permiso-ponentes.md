# Permiso de reproducción y publicación bajo licenca libre

Título de la presentación:
Nombre de quien presenta y firma:
Fecha de la presentación:

Por la presente doy mi consentimiento a la Universidad Rey Juan Carlos (URJC) para utilizar los materiales de la presentacion de título y fecha indicada, incluyendo las materiales o otros materiales utilizados, y los videos y fotos que se puedan haber realizado de ella. Esto incluye específicamente las fotografías y/o grabaciones de video que incluyan imágenes mías. Estas imágenes podrán usarse en formatos impresos como digitales, incluidas publicaciones impresas, sitios web, acciones y productos de marketing y publicidad, vídeos, medios sociales, acciones y recursos de enseñanza y con fines de investigación.

Otorgo también los materiales de la presentación de título y fecha indicada bajo la licencia marcada a continuación, sin que por ello deje de mantener sobre ellos mis derechos de autor que no sean cedidos expresamente por la licencia o que estén afectados por otros términos de este documento:

* [ ] Creative Commons Atribución 4.0 Internacional [1]
* [ ] Creative Commons Atribución-CompartirIgual 4.0 Internacional [2]

[1] [https://creativecommons.org/licenses/by/4.0/deed.es](https://creativecommons.org/licenses/by/4.0/deed.es)
[2] [https://creativecommons.org/licenses/by-sa/4.0/deed.es](https://creativecommons.org/licenses/by-sa/4.0/deed.es)

Entiendo que todos estos materiales podrán ser vistas en todo el mundo y que algunos países en el extranjero pueden no brindar el mismo nivel de protección a los derechos de los individuos como los establecidos en la legislación europea.

Los datos personales recogidos serán incorporados y tratados en los sistemas de tratamiento de datos de Gestión administrativa, académica y económica de las actividades docentes, investigadoras y de publicación desarrolladas en la Oficina de Conocimiento y Cultura Libres de la URJC, cuya finalidad es dar a conocer las opciones y ventajas de publicación libre del conocimiento generado en la Universidad Rey Juan Carlos. El tratamiento de los datos personales se lleva a cabo en virtud de los poderes públicos conferidos a la Universidad Rey Juan Carlos por la Ley y únicamente podrán ser cedidos en los supuestos previstos en la Ley. 

El órgano responsable del tratamiento es el Vicerrectorado de Calidad, Ética y Buen Gobierno, y la dirección donde el interesado podrá ejercer los derechos de acceso, rectificación, supresión, minimización del tratamiento, portabilidad y oposición ante el mismo es Oficina de Conocimiento y Cultura Libres (URJC), C/ Tulipán s/n, 28933-Móstoles, todo lo cual se informa en cumplimiento del artículo 13 del Reglamento Europeo 679/2016, de 27 de abril, general de protección de datos y el artículo 11 de la Ley Orgánica 3/2018, de 5 de diciembre, de Protección de Datos de Carácter Persona y garantía de los Derechos Digitales. 

Mediante mi firma, indico que consiento expresamente el tratamiento de mis datos personales para los fines anteriormente expresados, el tratamiento y gestión de mi imagen en las grabaciones efectuadas para los fines anteriormente expresados, y licencio mi obra de la forma anteriormente indicada.

Firmado en:
Fecha:
